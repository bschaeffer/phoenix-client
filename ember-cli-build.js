/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var env = EmberApp.env(),
      production = (env == 'production');

  var app = new EmberApp(defaults, {
    sassOptions: {
      extension: 'scss',
      includePaths: [
        'bower_components/bootstrap'
      ]
    },
    emberCliFontAwesome: {
      useScss: true
    },

    minifyJS: {
      enabled: production
    },
    minifyCSS: {
      enabled: production
    },

    autoprefixer: {
      browsers: ['last 2 chrome versions', 'last 2 safari versions'],
      cascade: false,
    }
  });

  app.import(app.bowerDirectory + '/js-quantities/src/quantities.js');
  app.import(app.bowerDirectory + '/typeahead.js/dist/typeahead.bundle.js');

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};

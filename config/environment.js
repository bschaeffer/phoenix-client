/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'phoenix',
    podModulePrefix: 'phoenix/pods',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',

    localSettings: {
      serializer: 'json',
      adapter: 'local-storage',
      prefix: 'pheonix-development/'
    },

    flashMessageDefaults: {
      timeout: 3000,
      extendedTimeout: 0,
      priority: 200,
      sticky: false,
      showProgress: false,

      // service defaults
      // type: 'info',
      // types: ['success', 'danger', 'warning'],
      injectionFactories: ['route', 'controller'],
      preventDuplicates: false
    },

    moment: {
      includeLocales: ['en']
    },

    APP: {
      apiEndpoint: 'http://api.jbnapi.dev'
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.apiEndpoint = null;
    ENV.APP.rootElement = '#ember-testing';

    ENV.localSettings.serializer = 'noop';
    ENV.localSettings.adapter = 'local-memory';
    ENV.localSettings.prefix = 'phoenix-test/';
  }

  if (environment === 'staging') {
    ENV.APP.apiEndpoint = "https://jbn-api-staging.herokuapp.com";
    ENV.localSettings.prefix = 'phoenix-staging/';
  }

  if (environment === 'production') {
    ENV.APP.apiEndpoint = "https://jbn-api-production.herokuapp.com";
    ENV.localSettings.prefix = 'phoenix/';
  }

  return ENV;
};

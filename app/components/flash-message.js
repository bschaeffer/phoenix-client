import FlashMessage from 'ember-cli-flash/components/flash-message';

export default FlashMessage.extend({

  didInsertElement() {
    this.$().fadeIn(200, () => {
      this._super();
    });
  },

  click() {
    this.$().animate({opacity: 0, height: 0}, 200, () => {
      this._destroyFlashMessage();
    });
  }
});

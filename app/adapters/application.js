import DS from 'ember-data';
import Ember from 'ember';
import ENV from 'phoenix/config/environment';

export default DS.JSONAPIAdapter.extend({
  currentUser: Ember.inject.service('current-user'),
  host: ENV.APP.apiEndpoint,
  headers: Ember.computed('currentUser.authHeaders', function() {
    return this.get('currentUser.authHeaders');
  }),

  pathForType() {
    return Ember.String.underscore(this._super(...arguments));
  }
});

import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  urlForFindRecord(id, modelType, snapshot) {
    return this._buildOurUrl(snapshot, true);
  },

  urlForCreateRecord(modelType, snapshot) {
    return this._buildOurUrl(snapshot, true);
  },

  urlForUpdateRecord(id, modelType, snapshot) {
    return this._buildOurUrl(snapshot, true);
  },

  urlForDeleteRecord(id, modelType, snapshot) {
    return this._buildOurUrl(snapshot, true);
  },

  _buildOurUrl(snapshot, withNutrient) {
    let record = snapshot.record;
    let url = this._buildURL('inventory', record.get('inventory.id'));

    if (withNutrient) {
      url += '/nutrients/' + record.get('nutrient.id');
    }

    return url;
  }
});

import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'span',
  state: '',

  defaultClass: 'label text-mono',

  classNameBindings: ['defaultClass',  'stateClass'],

  stateClass: Ember.computed('state', function() {
    return `label-${this.get('state')}`;
  })
});

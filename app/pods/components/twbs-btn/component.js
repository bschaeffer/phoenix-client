import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'button',

  defaultClass: 'btn',
  outline: false,
  disabled: false,
  active: false,
  'aria-pressed': Ember.computed.bool('active'),
  color: 'primary',
  type: 'button',
  sizing: null,
  text: 'Button',
  textDisabled: 'Button (disabled)',
  block: false,

  attributeBindings: ['disabled', 'type', 'aria-pressed'],
  classNameBindings: [
    'defaultClass',
    'colorClass',
    'sizeClass',
    'block:btn-block',
    'active'
  ],

  displayText: Ember.computed('text', 'textDisabled', 'disabled', function() {
    return this.get('disabled') ? this.get('textDisabled') : this.get('text');
  }),

  colorClass: Ember.computed('outline', 'color', function() {
    const klass = ['btn', this.get('color')];
    if (this.get('outline')) { klass.push('outline'); }
    return klass.join('-');
  }),

  sizeClass: Ember.computed('sizing', function() {
    if (this.get('sizing')) {
      return 'btn-' + this.get('sizing');
    }
  }),

  click(event) {
    this.sendAction('action', this.get('actionParam'));
    event.preventDefault();
    return false;
  }
});

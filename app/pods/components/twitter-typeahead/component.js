import Ember from 'ember';
/* globals Bloodhound */

let {
  get,
  compare,
  computed,
  computed: { gt, oneWay },
  observer,
  run
} = Ember;

export default Ember.Component.extend({
  classNameBindings: [':typeahead'],

  /*
    The bloodhound engine/
  */
  hound: null,

  /*
    Input options.
  */
  placeholder: '',
  type: 'text',
  value: '',
  searchValue: oneWay('value'),

  /*
    Bloodhound options.
  */
  sortBy: 'name',
  searchBy: 'name',
  identifyBy: 'id',

  results: null,
  hasResult: gt('results.length', 0),

  /*
    Initialize Bloodhound
  */
  didInsertElement() {
    Ember.run.scheduleOnce('afterRender', this, function() {
      const hound = new Bloodhound({
        local: this.get('content'),
        identify: this.identifyItem.bind(this),
        queryTokenizer: this.queryTokenizer.bind(this),
        datumTokenizer: this.datumTokenizer.bind(this),
        sorter: this.resultSorter.bind(this)
      });

      window.$hound = hound;
      this.set('hound', hound);
    });
  },

  performSearch: observer('searchValue', function() {
    let value = this.get('searchValue');
    this.get('hound').search(value, (results) => {
      this.set('results', results);
    });
  }),

  /*
    Reset hound when content changes
  */
  contentChanged: observer('content.[]', function() {
    const hound = this.get('hound');
    hound.local = this.get('content');
    hound.clear();
    hound.initialize(true);
  }),

  // Identify the token
  identifyItem(datum) {
    return get(datum, this.get('identifyBy'));
  },

  // Tokenize the query
  queryTokenizer(query) {
    let token = String(query).toLowerCase();
    return Bloodhound.tokenizers.whitespace(token);
  },

  // Tokenize the datum
  datumTokenizer(datum) {
    let token = get(datum, this.get('searchBy'));
    token = String(token).toLowerCase();
    return Bloodhound.tokenizers.whitespace(token);
  },

  resultSorter(a, b) {
    let sortBy = this.get('sortBy');
    compare(get(a, sortBy), get(b, sortBy));
  },

  actions: {
    focusOut() {
      this.set('results', null);
    },

    focusIn() {
      this.performSearch();
    }
  }
});

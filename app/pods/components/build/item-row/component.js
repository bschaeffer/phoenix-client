import Ember from 'ember';
import EditableRow from 'phoenix/mixins/editable-table/row';
import ItemsTable from '../items-table/component';

let {
  computed: { alias, or },
  inject: { service }
} = Ember;

const ItemsRow = Ember.Component.extend(EditableRow, {
  build: alias('model.build'),
  items: alias('table.items'),

  actions: {
    selectItem(item) {
      this.get('model').set('item', item);
    }
  }
});

ItemsRow.reopenClass({
  positionalParams: ['model']
});

export default ItemsRow;

import Ember from 'ember';

const BuildsTable = Ember.Component.extend({
  builds: Ember.computed.alias('builder.builds')
});

BuildsTable.reopenClass({
  positionalParams: ['builder']
});

export default BuildsTable;

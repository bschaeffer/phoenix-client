import Ember from 'ember';
import EditableTable from 'phoenix/mixins/editable-table/table';

let {
  computed,
  computed: { alias, union }
} = Ember;

const ItemsTable = Ember.Component.extend(EditableTable, {
  buildItems: alias('build.buildItems'),

  ingredients: computed(function() {
    return this.get('store').peekAll('ingredient');
  }).volatile(),

  recipes: computed(function() {
    return this.get('store').peekAll('recipe');
  }).volatile(),

  items: union('ingredients', 'recipes'),

  isEditable: alias('build.isPrototype'),

  actions: {
    addItem() {
      const build = this.get('build');
      const item = this.get('store').createRecord('build-item', {
        build: build,
      });

      build.get('buildItems').pushObject(item);
    }
  }
});

ItemsTable.reopenClass({
  positionalParams: ['build']
});

export default ItemsTable;

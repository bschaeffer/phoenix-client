import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'th',

  text: null,
  param: null,
  value: null,
  order: null,
  defaultOrder: 'asc',

  classNameBindings: [':clickable', 'active'],

  active: Ember.computed('param', 'value', function() {
    return this.get('param') === this.get('value');
  }),

  isAscending: Ember.computed.equal('order', 'asc'),
  isDescending: Ember.computed.equal('order', 'desc'),

  sortIcon: Ember.computed('active', 'isAscending', function() {
    if (!this.get('active')) {
      return 'sort';
    } else {
      return this.get('isAscending') ? 'sort-asc' : 'sort-desc';
    }
  }),

  toggleOrder() {
    if (this.get('isAscending')) {
      this.set('order', 'desc');
    } else {
      this.set('order', 'asc');
    }
  },

  click(event) {
    if (this.get('active')) {
      this.toggleOrder();
    } else {
      this.setProperties({
        param: this.get('value'),
        order: this.get('defaultOrder')
      });
    }

    event.preventDefault();
    return false;
  }
});

import Ember from 'ember';

const TypeLabel = Ember.Component.extend({
  tagName: 'div',

  classNameBindings: ['defaultClass', 'iconClass'],

  defaultClass: 'type-icon',

  iconClass: Ember.computed('model.modelType', function() {
    return `type-icon-${this.get('model.modelType')}`;
  }),

  icon: Ember.computed('model.modelType', function() {
    const type = this.get('model.modelType');
    if (Ember.isEmpty(type)) {
      return '-';
    } else {
      return type.charAt(0).toUpperCase();
    }
  })
});

TypeLabel.reopenClass({
  positionalParams: ['model']
});

export default TypeLabel;

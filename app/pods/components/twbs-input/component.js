import Ember from 'ember';

export default Ember.TextField.extend({
  sizing: null,
  focus: false,
  defaultClass: 'form-control',

  classNameBindings: ['defaultClass', 'sizeClass'],

  sizeClass: Ember.computed('sizing', function() {
    if (this.get('sizing')) {
      return 'form-control-' + this.get('sizing');
    }
  }),

  didInsertElement() {
    if (this.get('focus')) {
      this.$().focus();
    }
  }
});

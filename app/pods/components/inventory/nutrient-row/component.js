import Ember from 'ember';
import UNITS from 'phoenix/constants/unitwise-units';
import NutrientsTable from '../nutrients-table/component';
import Validations from 'phoenix/mixins/validations/inventory-nutrient';

let TASupport = Ember.TargetActionSupport;

const NutrientRow = Ember.Component.extend(Validations, TASupport, {
  store: Ember.inject.service(),
  flash: Ember.inject.service('flash'),

  tagName: 'tr',
  classNameBindings: ['isEditing'],

  isEditing: false,
  isSaving: false,
  isDeleting: false,
  isDisabled: Ember.computed.or('isSaving', 'isDeleting'),

  unitwiseUnits: UNITS,
  inventory: Ember.computed.alias('model.inventory'),
  nutrients: Ember.computed('inventory.nutrients.@each.nutrient.id', 'model.nutrient.id', function() {
    let nutrients = this.get('store').peekAll('nutrient').sortBy('name');
    let currentId = this.get('model.nutrient.id');
    let usedIds = this.get('inventory.nutrients')
      .mapBy('nutrient.id')
      .filter((i) => i);

    return nutrients.filter(function(n) {
      let nutrientId = n.get('id');
      return nutrientId === currentId || !usedIds.contains(nutrientId);
    });
  }),

  didInsertElement() {
    Ember.run.scheduleOnce('afterRender', this, 'registerWithTable');
    this._super(...arguments);
  },

  registerWithTable() {
    if (this.get('model.isNew')) {
      this.set('isEditing', true);
    }

    let table = this.nearestOfType(NutrientsTable);
    if (table) {
      this.set('table', table);
      table.get('tableRows').addObject(this);
    }
  },

  willDestroyElement() {
    let table = this.nearestOfType(NutrientsTable);
    if (table) { table.get('tableRows').removeObject(this); }
  },

  actions: {
    selectNutrient(nutrient) {
      this.get('model').set('nutrient', nutrient);
    },

    deleteNutrient() {
      this.set('isDeleting', true);

      this.get('model').destroyRecord().then(() => {
        this.get('table.tableRows').removeObject(this);
      }, (error) => {
        this.get('flash').displayAdapterError(error);
      }).finally(() => {
        this.set('isDeleting', false);
      });
    },

    saveNutrient() {
      const flash = this.get('flash');
      const model = this.get('model');
      this.set('isSaving', false);

      this.validate().then(function() {
        return model.save();
      }).then(() => {
        this.set('isEditing', false);
      }, function(error) {
        flash.displayAdapterError(error);
      }).finally(() => {
        this.set('isSaving', false);
      });
    },

    showEditor() {
      this.set('isEditing', true);
      return false;
    },

    cancelEditor() {
      let model = this.get('model');
      if (model.get('isNew')) {
        model.destroyRecord();
        this.get('table.tableRows').removeObject(this);
      } else {
        model.rollbackAttributes();
      }

      this.set('isEditing', false);
    }
  }
});

NutrientRow.reopenClass({
  positionalParams: ['model'],
});

export default NutrientRow;

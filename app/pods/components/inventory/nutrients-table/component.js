import Ember from 'ember';

let {
  computed,
  computed: { not },
  inject: { service },
} = Ember;

const NutrientTable = Ember.Component.extend({
  store: service(),

  nutrients: computed(function() {
    return this.get('store').peekAll('nutrient');
  }).volatile(),

  tableRows: computed(function() {
    return Ember.A();
  }),

  isEditable: not('inventory.isRecorded'),

  canAddMore: computed('inventory.nutrients.length', 'nutrients.length', function() {
    return this.get('inventory.nutrients.length') < this.get('nutrients.length');
  }),

  isEditing: computed('tableRows.@each.isEditing', function() {
    return this.get('tableRows').isAny('isEditing', true);
  }),

  isDisabled: computed('tableRows.@each.isDisabled', function() {
    return this.get('tableRows').isEvery('isDisabled', true);
  }),

  actions: {
    addNutrient() {
      const inventory = this.get('inventory');
      const nutrient = this.get('store').createRecord('inventory-nutrient', {
        inventory: inventory
      });

      inventory.get('nutrients').pushObject(nutrient);
    },

    saveAllNutrients() {
      this.get('tableRows').forEach(function(tableRow) {
        if (!tableRow.get('isSaving') || !tableRow.get('isDeleting')) {
          tableRow.triggerAction({
            action: 'saveNutrient',
            target: tableRow
          });
        }
      });
    }
  }
});

NutrientTable.reopenClass({
  positionalParams: ['inventory']
});

export default NutrientTable;

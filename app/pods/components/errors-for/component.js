import Ember from 'ember';

const ErrorComponent = Ember.Component.extend({
  tagName: 'small',
  classNameBindings: [':error-message',  'hasError:errored', ':text-danger'],

  field: undefined,
  fieldName: null,

  displayField: Ember.computed('field', 'fieldName', function() {
    const fieldName = this.get('fieldName');

    if (!Ember.isEmpty(fieldName)) {
      return fieldName;
    } else {
      return this.get('field').dasherize().replace('-', ' ').capitalize();
    }
  }),

  fieldErrors: Ember.computed('vController.fieldErrors', 'field', function() {
    return Ember.Object.extend({
      errors: Ember.computed.alias(`parent.model.${this.get('field')}`)
    }).create({parent: this.get('vController.fieldErrors')});
  }),

  modelErrors: Ember.computed('vController.model.errors', 'field', function() {
    return Ember.Object.extend({
      errors: Ember.computed.alias(`parent.${this.get('field')}`)
    }).create({parent: this.get('vController.model.errors')});
  }),

  formErrors: Ember.computed.collect(
    'fieldErrors.errors.firstObject',
    'modelErrors.errors.firstObject.message'
  ),

  hasError: Ember.computed.notEmpty('formErrors.firstObject'),

  errorMessage: Ember.computed('displayField', 'formErrors.firstObject', function() {
    return `${this.get('displayField')} ${this.get('formErrors.firstObject')}.`;
  })
});

ErrorComponent.reopenClass({
  positionalParams: ['vController', 'field', 'fieldName']
});

export default ErrorComponent;

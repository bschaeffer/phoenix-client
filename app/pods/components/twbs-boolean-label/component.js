import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'span',
  value: false,
  pill: true,

  defaultClass: 'label',
  classNameBindings: ['defaultClass',  'pillClass', 'colorClass'],

  pillClass: Ember.computed('pill', function() {
    return !this.get('pill') ? null : 'label-pill';
  }),

  colorClass: Ember.computed('value', function() {
    return !this.get('value') ? 'label-default' : 'label-success';
  }),

  displayText: Ember.computed('value', function() {
    return !this.get('value') ? 'No' : 'Yes';
  })
});

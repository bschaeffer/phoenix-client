import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'progress',

  remaining: 0,
  total: 100,

  max: 100,

  value: Ember.computed('remaining', 'total', function() {
    return (this.get('remaining') / this.get('total')) * 100;
  }),

  defaultClass: 'progress m-b-0',

  innerStyle: Ember.computed('value', function() {
    return Ember.String.htmlSafe(`width: ${Number(this.get('value'))}%;`);
  }),

  attributeBindings: ['value', 'max'],
  classNameBindings: ['defaultClass', 'colorClass'],

  colorClass: Ember.computed('value', function() {
    let percent = this.get('value');
    if (percent >= 75) {
      return 'progress-success';
    } else if (percent >= 50) {
      return 'progress-warning';
    } else if (percent > 0) {
      return 'progress-danger';
    }
  })
});

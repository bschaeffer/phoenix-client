import Ember from 'ember';

export default Ember.Route.extend({

  beforeModel(transition) {
    const currentUser = this.get('currentUser');
    const route = this;

    return currentUser.ensure().then(() => {
      transition.abort();
      currentUser.redirectOr(function() {
        route.transitionTo('application');
      });
    }).catch(function() { });
  },

  actions: {
    login() {
      const route = this;
      const login = this.get('controller');
      const currentUser = this.get('currentUser');

      login.setProperties({
        isLoggingIn: true,
        credentialsError: null
      });

      login.validate().then(function() {
        const email = login.get('email');
        const password = login.get('password');
        const auth = currentUser.authenticate(email, password);

        auth.then(function() {
          login.setProperties({
            _hasValidated: false,
            isLoggingIn: false,
            password: null,
            credentialsError: null
          });

          currentUser.redirectOr(function() {
            route.transitionTo('application');
          });

        }, function() {
          login.setProperties({
            isLoggingIn: false,
            credentialsError: 'Email or password is invalid'
          });
        });

      }).catch(function() {
        login.setProperties({
          isLoggingIn: false,
          credentialsError: null
        });
      });
    }
  }
});

import Ember from 'ember';
import LazyValidations from 'phoenix/mixins/lazy-validations';

export default Ember.Controller.extend(LazyValidations, {
  email: null,
  password: null,
  credentialsError: null,
  isLoggingIn: false,

  emailErrors: Ember.computed.notEmpty('fieldErrors.email'),
  passwordErrors: Ember.computed.notEmpty('fieldErrors.password'),

  validations: {
    'email': {
      presence: true,
      email: true
    },
    'password': {
      presence: true,
      length: {
        minimum: 8,
        maximum: 50
      }
    },
  }
});

import Ember from 'ember';

export default Ember.Route.extend({

  beforeModel() {
    const currentUser = this.get('currentUser');

    currentUser.logout().then(function() {
      window.location = '/auth/login';
    });

    return new Ember.RSVP.Promise(function() {});
  },
});

import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'Edit',
    linkable: false,
  },

  controllerName: 'app/recipes/new',
  templateName: 'app/recipes/form',

  model(params) {
    return this.get('store').peekRecord('recipe', params.recipe_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', `${model.get('name')} (Edit)`);
  },

  actions: {
    saveRecipe() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((recipe) => {
          this.transitionTo('app.recipes.show', recipe).then(() => {
            flash.success('Recipe saved!');
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    willTransition() {
      const model = this.get('controller.model');
      model.rollbackAttributes();
    }
  }
});

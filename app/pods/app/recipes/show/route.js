import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: null,
    linkable: true
  },

  model(params) {
    return this.get('store').peekRecord('recipe', params.recipe_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', model.get('name'));
  }
});

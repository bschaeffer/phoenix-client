import Ember from 'ember';
import Named from 'phoenix/mixins/named-controller';
import Paged from 'phoenix/mixins/paged-controller';
import Sorted from 'phoenix/mixins/sorted-controller';

export default Ember.Controller.extend(Named, Paged, Sorted, {
  queryParams: ['name'],

  sort: 'name',
  order: 'asc',

  sortedContentProxy: Ember.computed.oneWay('nameFilteredContent'),
  pagedContentProxy: Ember.computed.oneWay('sortedContent'),

  changeName(name) {
    this.setProperties({name: name, page: 1});
  }
});

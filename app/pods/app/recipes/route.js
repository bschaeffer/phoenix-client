import Ember from 'ember';

export default Ember.Route.extend({

  actions: {
    cancelEditing(model) {
      if (model.get('isNew')) {
        this.transitionTo('app.recipes');
      } else {
        this.transitionTo('app.recipes.show', model);
      }
    },

    deleteRecipe(recipe, redirect) {
      const flash = this.get('flash');

      if(confirm('Are you sure you want to delete this recipe?')) {
        recipe.destroyRecord().then(() => {
          flash.info(`Recipe "${recipe.get('name')}" has been removed.`);
          if (redirect) {
            this.transitionTo('app.recipes');
          }
        }, function(error) {
          flash.displayAdapterError(error);
        });
      }
    }
  }
});

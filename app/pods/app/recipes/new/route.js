import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'New Recipe',
    linkable: false
  },

  templateName: 'app/recipes/form',

  model() {
    return this.get('store').createRecord('recipe');
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  actions: {
    willTransition() {
      const model = this.get('controller.model');
      if (model.get('isNew')) { model.destroyRecord(); }
    }
  }
});

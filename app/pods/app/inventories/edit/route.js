import Ember from 'ember';

export default Ember.Route.extend({
  controllerName: 'app/inventories/new',

  breadCrumb: {
    title: 'Inventory (Edit)',
    linkable: false
  },

  model(params) {
    return this.get('store').findRecord('inventory', params.inventory_id);
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/inventories/form', {controller: controller});
  },

  actions: {
    saveInventory() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((inventory) => {
          this.transitionTo('app.inventories.show', inventory).then(() => {
            flash.success('Inventory updated!');
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    cancelEditing() {
      const model = this.get('controller.model');
      this.transitionTo('app.inventories.show', model);
    },

    willTransition() {
      const model = this.get('controller.model');
      model.rollbackAttributes();
    }
  }
});

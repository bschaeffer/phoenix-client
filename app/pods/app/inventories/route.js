import Ember from 'ember';

export default Ember.Route.extend({

  actions: {
    deleteInventory(inventory, redirect) {
      const flash = this.get('flash');

      if (inventory.get('isRecorded')) {
        flash.danger('You cannot delete inventory that has been entered in record.');
      } else if(confirm('Are you sure you want to delete this inventory?')) {
        inventory.destroyRecord().then(() => {
          flash.info(`Inventory has been removed.`);
          if (redirect) {
            this.transitionTo('app.inventories');
          }
        }, function(error) {
          flash.displayAdapterError(error);
        });
      }
    },

    recordInventory(inventory) {
      const flash = this.get('flash');
      const message = 'Are you sure you want to record this inventory?' +
        ' This adds the inventory amount to the adjustment record and you' +
        ' will no longer be able to edit it or its nutrients.';

      if (inventory.get('isRecorded')) {
        flash.info('This inventory has already been entered into record.');
      } else if (confirm(message)) {
        inventory.enterIntoRecord().then(() => {
          flash.success('Inventory has been entered into the record.');
        }).catch(function(error) {
          flash.displayAdapterError(error);
        });
      }

      return false;
    },
  }
});

import Ember from 'ember';
import Sorted from 'phoenix/mixins/sorted-controller';

export default Ember.Controller.extend(Sorted, {
  queryParams: ['state', 'sort', 'order', 'ingredient_id'],

  state: null,
  sort: 'purchasedOn',
  order: 'desc',
  ingredient_id: null,

  _ingredients: Ember.computed(function() {
    return this.get('store').peekAll('ingredient').sortBy('name');
  }),

  ingredients: function() {
    return this.get('_ingredients').filter(function(item) {
      return item.get('inventoryCount') > 0;
    });
  }.property('_ingredients.@each.inventoryCount'),

  filteredContent: function() {
    let state = this.get('state');
    let ingredientId = this.get('ingredient_id');

    let filtered = this.get('model');

    if (!Ember.isEmpty(state)) {
      filtered = filtered.filterBy('state', state);
    }

    if (!Ember.isEmpty(ingredientId)) {
      filtered = filtered.filterBy('ingredient.id', ingredientId);
    }

    return filtered;
  }.property('model', 'state', 'ingredient_id'),

  sortedContentProxy: Ember.computed.alias('filteredContent'),

  actions: {
    clearFilter() {
      this.set('ingredient_id', null);
    }
  }
});

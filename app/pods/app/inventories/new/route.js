import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'New Inventory',
    linkable: false
  },

  model() {
    return this.get('store').createRecord('inventory');
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/inventories/form', {controller: controller});
  },

  actions: {
    saveInventory() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((inventory) => {
          this.transitionTo('app.inventories.show', inventory).then(function() {
            flash.success('Inventory created!');
          });
        });
      }).catch(function(error) {
        console.log({error});
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    cancelEditing() {
      this.transitionTo('app.inventories');
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('isNew')) {
        model.destroyRecord();
      }
    }
  }
});

Ember.onerror = function() { console.log(arguments); };

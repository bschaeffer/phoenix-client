import Ember from 'ember';
import UNITS from 'phoenix/constants/unitwise-units';
import LazyValidations from 'phoenix/mixins/lazy-validations';
import moment from 'moment';

export default Ember.Controller.extend(LazyValidations, {
  validations: {
    'model.purchasedOn': {
      presence: true
    },
    'model.lotNumber': {
      presence: true
    },
    'model.ingredient': {
      belongsTo: true,
    },
    'model.measureValue': {
      presence: true,
      measured: 'ingredient'
    },
    'model.price': {
      presence: true,
      numericality: true
    },
    'model.measureUnit': {
      presence: {
        'if': function(object) {
          return !!object.get('model.ingredient.measureable');
        }
      }
    },
    'model.expiresOn': {
      presence: {
        'if': function(object) {
          return !!object.get('model.ingredient.expireable');
        }
      }
    },
  },

  todaysDate: Ember.computed(function() {
    return moment().toDate();
  }),

  unitwiseUnits: UNITS,

  ingredients: Ember.computed(function() {
    let ingredients = this.get('store').peekAll('ingredient').sortBy('name');
    return ingredients;
  }),

  actions: {
    selectIngredient(ingredient) {
      this.get('model').set('ingredient', ingredient);
    }
  }
});

import Ember from 'ember';

export default Ember.Route.extend({
  controllerName: 'app/products/new',

  breadCrumb: {
    title: null,
    linkable: false
  },

  model(params) {
    return this.get('store').peekRecord('product', params.product_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', `${model.get('name')} (Edit)`);
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/products/form', {controller});
  },

  actions: {
    cancelEditing() {
      const model = this.get('controller.model');
      this.transitionTo('app.products.show', model);
    },

    willTransition() {
      const model = this.get('controller.model');
      model.rollbackAttributes();
    }
  }
});

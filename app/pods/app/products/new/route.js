import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'New Product',
    linkable: false
  },

  model() {
    return this.get('store').createRecord('product');
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/products/form', {controller});
  },

  actions: {
    cancelEditing() {
      this.transitionTo('app.products');
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('isNew')) {
        model.destroyRecord();
      }
    }
  }
});

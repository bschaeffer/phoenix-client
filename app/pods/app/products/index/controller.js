import Ember from 'ember';
import Sorted from 'phoenix/mixins/sorted-controller';

export default Ember.Controller.extend(Sorted, {
  queryParams: ['name'],

  sort: 'name',
  order: 'asc',

  name: '',

  filteredContent: function() {
    let name = this.get('name');

    let filtered = this.get('model');

    if (!Ember.isEmpty(name)) {
      const nameRegex = new RegExp(name, 'i');
      filtered = filtered.filter(function(product) {
        return nameRegex.test(product.get('name'));
      });
    }

    return filtered;
  }.property('model', 'name'),

  sortedContentProxy: Ember.computed.alias('filteredContent'),
});

import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    saveProduct(product) {
      let controller, message;

      if (product.get('isNew')) {
        controller = this.controllerFor('app/products/new');
        message = "Product created!";
      } else {
        controller = this.controllerFor('app/products/edit');
        message = "Product updated!";
      }

      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);
      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((product) => {
          this.transitionTo('app.products.show', product).then(function() {
            flash.success(message);
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    deleteProduct(product, redirect) {
      const flash = this.get('flash');

      if (confirm('Are you sure you want to delete this product?')) {
        product.destroyRecord().then(() => {
          flash.info('Product has been deleted.');
          if (redirect) {
            this.transitionTo('app.products');
          }
        }, function(error) {
          flash.displayAdapterError(error);
        });
      }
    }
  }
});

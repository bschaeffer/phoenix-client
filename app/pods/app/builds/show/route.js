import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: null,
    linkable: true
  },

  model(params) {
    return this.get('store').findRecord('build', params.build_id);
  },

  afterModel(model) {
    let title = `${model.get('builder.name')} - #${model.get('version')}`
    this.set('breadCrumb.title', title);
  }
});

import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: null,
    linkable: false,
  },

  model(params) {
    const store = this.get('store');
    return store.peekRecord(params.builder_type, params.builder_id);
  },

  setupController(controller, builder) {
    const build = this.get('store').createRecord('build', {
      builder: builder,
      version: builder.get('versions') + 1
    });

    controller.set('model', build);
    let title = `New Build #${build.get('version')} for ${builder.get('name')}`
    this.set('breadCrumb.title', title);
  },

  serialize(model) {
    return {
      builder_type: model.get('modelType'),
      builder_id: model.get('id')
    };
  },

  actions: {
    willTransition() {
      const model = this.get('controller.model');
      model.destroyRecord();
    }
  }
});

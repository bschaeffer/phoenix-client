import Ember from 'ember';

export default Ember.Route.extend({
  controllerName: 'app/nutrients/new',

  breadCrumb: {
    title: null,
    linkable: false
  },

  model(params) {
    return this.get('store').peekRecord('nutrient', params.nutrient_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', `${model.get('name')} (Edit)`);
  },

  setupController(controller) {
    let nutrientCategories = this.get('store')
      .peekAll('nutrient_category')
      .sortBy('name');

    controller.set('nutrientCategories', nutrientCategories);
    controller.cleanupValidations();
    this._super(...arguments);
  },

  renderTemplate(controller) {
    this.render('app/nutrients/form', {controller: controller});
  },

  actions: {
    saveNutrient() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((nutrient) => {
          this.transitionTo('app.nutrients.show', nutrient).then(() => {
            flash.success('Nutrient saved!');
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    cancelEditing() {
      const model = this.get('controller.model');
      this.transitionTo('app.nutrients.show', model);
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('hasDirtyAttributes')) {
        model.rollbackAttributes();
      }
    }
  }
});

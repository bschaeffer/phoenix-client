import Ember from 'ember';

export default Ember.Route.extend({
  controllerName: 'app/nutrients/categories/new',

  breadCrumb: {
    title: 'New Category',
    linkable: false
  },

  model() {
    return this.get('store').createRecord('nutrient_category');
  },

  setupController(controller) {
    controller.cleanupValidations();
    this._super(...arguments);
  },

  renderTemplate(controller) {
    this.render('app/nutrients/categories/form', {controller});
  },

  actions: {
    saveCategory() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((category) => {
          this.transitionTo('app.nutrients.categories.show', category).then(() => {
            flash.success('Category saved!');
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    cancelEditing() {
      const model = this.get('controller.model');
      this.transitionTo('app.nutrients.categories.show', model);
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('hasDirtyAttributes')) {
        model.rollbackAttributes();
      }
    }
  }
});

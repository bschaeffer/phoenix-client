import Ember from 'ember';
import Paged from 'phoenix/mixins/paged-controller';
import Sorted from 'phoenix/mixins/sorted-controller';

export default Ember.Controller.extend(Paged, Sorted, {
  sort: 'name',

  pagedContentProxy: Ember.computed.oneWay('sortedContent'),
});

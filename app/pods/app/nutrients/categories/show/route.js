import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: null,
    linkable: false
  },

  model(params) {
    return this.get('store').peekRecord('nutrient_category', params.category_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', model.get('name'));
  }
});

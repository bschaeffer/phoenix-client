import Ember from 'ember';
import LazyValidations from 'phoenix/mixins/lazy-validations';

export default Ember.Controller.extend(LazyValidations, {
  validations: {
    'model.name': {
      presence: true
    }
  }
});

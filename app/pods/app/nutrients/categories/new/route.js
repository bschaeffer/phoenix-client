import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'New Category',
    linkable: false
  },

  model() {
    return this.get('store').createRecord('nutrient_category');
  },

  setupController(controller) {
    controller.cleanupValidations();
    this._super(...arguments);
  },

  renderTemplate(controller) {
    this.render('app/nutrients/categories/form', {controller});
  },

  actions: {
    saveCategory() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then(() => {
          this.transitionTo('app.nutrients.categories').then(() => {
            flash.success('Category created!');
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    cancelEditing() {
      this.transitionTo('app.nutrients.categories');
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('isNew')) {
        model.destroyRecord();
      }
    }
  }
});

import Ember from 'ember';

export default Ember.Route.extend({

  model() {
    return this.get('store').peekAll('nutrient');
  },

  renderTemplate() {
    this._super();
    this.render("app/nutrients/header", {outlet: "nutrient-header"});

    this.render("app/nutrients/header_buttons", {
      into: "app/nutrients/header",
      outlet: "nutrient-header-buttons"
    });
  }
});

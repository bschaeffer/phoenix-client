import Ember from 'ember';
import Paged from 'phoenix/mixins/paged-controller';
import Sorted from 'phoenix/mixins/sorted-controller';

export default Ember.Controller.extend(Paged, Sorted, {
  queryParams: ['name'],

  name: null,
  nameProxy: Ember.computed.oneWay('name'),

  sort: 'name',

  pagedContentProxy: Ember.computed.oneWay('sortedContent'),

  nameObserver: Ember.observer('nameProxy', function() {
    Ember.run.debounce(this, this.changeName, this.get('nameProxy'), 450);
  }),

  changeName(name) {
    this.setProperties({name: name, page: 1});
  }
});

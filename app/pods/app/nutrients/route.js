import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    deleteNutrient(nutrient, redirect) {
      const flash = this.get('flash');

      if (confirm('Are you sure you want to delete this nutrient? This cannot be undone?')) {
        nutrient.destroyRecord().then(() => {
          flash.warning(`${nutrient.get('name')} deleted.`);
          if (redirect) {
            this.transitionTo('app.nutrients');
          }
        }).catch(function(error) {
          flash.displayAdapterError(error);
        });
      }
    },

    deleteCategory(category, redirect) {
      const flash = this.get('flash');

      if (confirm('Are you sure you want to delete this category? This cannot be undone?')) {
        category.destroyRecord().then(() => {
          flash.info(`${category.get('name')} deleted.`);
          if (redirect) {
            this.transitionTo('app.nutrients.categories');
          }
        }, function(error) {
          flash.displayAdapterError(error);
        });
      }
    }
  }
});

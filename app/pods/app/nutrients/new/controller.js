import Ember from 'ember';
import LazyValidations from 'phoenix/mixins/lazy-validations';
import UNITS from 'phoenix/constants/unitwise-units';

export default Ember.Controller.extend(LazyValidations, {
  validations: {
    'model.name': {
      presence: true
    },
    'model.defaultUnit': {
      presence: true,
      unitwise: true
    },
    'model.nutrientCategory': {
      belongsTo: true
    }
  },

  unitwiseUnits: UNITS,

  nutrientCategories: Ember.computed(function() {
    return this.get('store').peekAll('nutrient_category').sortBy('name');
  }).volatile(),

  actions: {
    selectNutrientCategory(category) {
      this.get('model').set('nutrientCategory', category);
    }
  }
});

import Ember from 'ember';
import UNITS from 'phoenix/constants/unitwise-units';

export default Ember.Route.extend({
  controllerName: 'app/nutrients/new',

  breadCrumb: {
    title: 'New Nutrient',
    linkable: false
  },

  model() {
    return this.get('store').createRecord('nutrient', {
      defaultUnit: UNITS[0]
    });
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/nutrients/form', {controller: controller});
  },

  actions: {
    saveNutrient() {
      const controller = this.get('controller');
      const model = controller.get('model');
      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return model.save().then((nutrient) => {
          this.transitionTo('app.nutrients.show', nutrient).then(() => {
            flash.success('Nutrient created!');
          });
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    cancelEditing() {
      this.transitionTo('app.nutrients');
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('isNew')) {
        model.destroyRecord();
      }
    }
  }
});

import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: null,

  beforeModel(transition) {
    if (!this.get('currentUser.isSignedIn')) {
      transition.abort();
      this.set('currentUser.attemptedTransition', transition);
      this.transitionTo('auth.login');
    }
  },

  model() {
    const store = this.get('store');
    return Ember.RSVP.hash({
      items: store.findAll('item'),
      nutrientCategories: store.findAll('nutrient_category'),
      nutrients: store.findAll('nutrient'),
      products: store.findAll('product')
    });
  },

  actions: {
    error(error) {
      this.get('flash').displayAdapterError(error);
      return true;
    }
  }
});

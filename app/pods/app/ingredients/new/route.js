import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'New Ingredient',
    linkable: false
  },

  model() {
    return this.get('store').createRecord('ingredient', {
      budgetable: true,
      measureable: true,
      expireable: false
    });
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/ingredients/form', {controller: controller});
  },

  actions: {
    cancelEditing() {
      this.transitionTo('app.ingredients');
    },

    willTransition() {
      const model = this.get('controller.model');
      if (model.get('isNew')) {
        model.destroyRecord();
      }
    }
  }
});

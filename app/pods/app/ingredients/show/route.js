import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: null,
    linkable: true
  },

  model(params) {
    const store = this.get('store');
    return store.findRecord('ingredient', params.ingredient_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', model.get('name'));
  }
});

import Ember from 'ember';

export default Ember.Route.extend({
  controllerName: 'app/ingredients/new',

  breadCrumb: {
    title: null,
    linkable: false
  },

  model(params) {
    return this.get('store').peekRecord('ingredient', params.ingredient_id);
  },

  afterModel(model) {
    this.set('breadCrumb.title', `${model.get('name')} (Edit)`);
  },

  setupController(controller) {
    this._super(...arguments);
    controller.cleanupValidations();
  },

  renderTemplate(controller) {
    this.render('app/ingredients/form', {controller: controller});
  },

  actions: {
    cancelEditing() {
      const model = this.get('controller.model');
      this.transitionTo('app.ingredients.show', model);
    },

    willTransition() {
      const model = this.get('controller.model');
      model.rollbackAttributes();
    }
  }
});

import Ember from 'ember';
import Named from 'phoenix/mixins/named-controller';
import Paged from 'phoenix/mixins/paged-controller';
import Sorted from 'phoenix/mixins/sorted-controller';

export default Ember.Controller.extend(Named, Paged, Sorted, {
  queryParams: ['name'],

  sort: 'name',
  order: 'asc',

  sortedContentProxy: Ember.computed.alias('nameFilteredContent'),
  pagedContentProxy: Ember.computed.alias('sortedContent'),

  resetPage: Ember.on('nameDidChange', function() {
    this.set('page', 1);
  })
});

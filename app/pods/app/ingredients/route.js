import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: {
    title: 'Ingredients',
    linkable: true
  },

  actions: {
    saveIngredient(ingredient) {
      let controller, message;

      if (ingredient.get('isNew')) {
        controller = this.controllerFor('app/ingredients/new');
        message = "Ingredient created!";
      } else {
        controller = this.controllerFor('app/ingredients/edit');
        message = "Ingredient updated!";
      }

      const flash = this.get('flash');

      controller.set('isSaving', true);

      controller.validate().then(() => {
        controller.cleanupValidations();
        return ingredient.save().then((ingredient) => {
          this.transitionTo('app.ingredients.show', ingredient).then(() => {
            flash.success('Ingredient created!');
          });
        }, function(error) {
          flash.displayAdapterError(error);
        });
      }).finally(function() {
        controller.set('isSaving', false);
      });
    },

    deleteIngredient(ingredient, redirect) {
      const flash = this.get('flash');
      if (confirm('Are you sure you want to delete this ingredient? This cannot be undone?')) {
        ingredient.destroyRecord().then(() => {
          flash.info(`${ingredient.get('name')} deleted.`);
          if (redirect) {
            this.transitionTo('app.ingredients');
          }
        }, function(error) {
          flash.displayAdapterError(error);
        });
      }

      return false;
    }
  }
});

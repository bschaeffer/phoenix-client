import DS from 'ember-data';
import BaseModel from 'phoenix/mixins/model';

export default DS.Model.extend(BaseModel, {
  nutrients: DS.hasMany('nutrient'),

  name: DS.attr('string'),
  nutrientsCount: DS.attr('number')
});

import DS from 'ember-data';
import BaseModel from 'phoenix/mixins/model';

export default DS.Model.extend(BaseModel, {

  nutrientCategory: DS.belongsTo('nutrient_category'),

  name: DS.attr('string'),
  standard: DS.attr('boolean'),
  defaultUnit: DS.attr('string')
});

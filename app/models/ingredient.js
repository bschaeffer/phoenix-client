import Ember from 'ember';
import DS from 'ember-data';
import BaseModel from 'phoenix/mixins/model';

export default DS.Model.extend(BaseModel, {
  name: DS.attr(),
  labelName: DS.attr(),
  budgetable: DS.attr('boolean'),
  expireable: DS.attr('boolean'),
  measureable: DS.attr('boolean'),

  inventories: DS.hasMany('inventory'),
  inventoryCount: Ember.computed.alias('inventories.length')
});

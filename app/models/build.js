import DS from 'ember-data';
import Base from 'phoenix/mixins/model';

let  { belongsTo, hasMany, attr } = DS;

let {
  computed: { equal }
} = Ember;

export default DS.Model.extend(Base, {
  builder: belongsTo('builder'),
  parent: belongsTo('build'),
  buildItems: hasMany('build-item'),

  state: attr('string', {defaultValue: 'prototype'}),
  version: attr('string'),
  decidedOn: attr(),

  isPrototype: equal('state', 'prototype'),
  isTesting: equal('state', 'testing'),
  isRejected: equal('state', 'rejected'),
  isFinalized: equal('state', 'finalized')
});

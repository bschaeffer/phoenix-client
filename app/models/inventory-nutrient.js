import Ember from 'ember';
import DS from 'ember-data';
import Base from 'phoenix/mixins/model';
import Measureable from 'phoenix/mixins/measureable-model';
/* globals Qty */

export default DS.Model.extend(Base, Measureable, {
  inventory: DS.belongsTo('inventory'),
  nutrient: DS.belongsTo('nutrient'),

  convertedMeasure: Ember.computed('inventory.measure', 'measure', function() {
    let measure = this.get('measure');
    let inventoryMeasure = this.get('inventory.measure');

    if (measure.isCompatible(inventoryMeasure)) {
      return measure.to(inventoryMeasure);
    } else {
      return new Qty(measure);
    }
  }),

  rate: Ember.computed('inventory.measureValue', 'convertedMeasure', function() {
    const inventoryValue = this.get('inventory.measureValue');
    const convertedMeasure = this.get('convertedMeasure');
    return (convertedMeasure.scalar / inventoryValue);
  })
});

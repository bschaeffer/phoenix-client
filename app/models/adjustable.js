import DS from 'ember-data';
import Base from 'phoenix/mixins/model';

export default DS.Model.extend(Base);

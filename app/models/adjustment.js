import DS from 'ember-data';
import Base from 'phoenix/mixins/model';

export default DS.Model.extend(Base, {
  inventory: DS.belongsTo('inventory'),
  adjustable: DS.belongsTo('adjustable'),

  action: DS.attr('string'),
  amount: DS.attr('number')
});

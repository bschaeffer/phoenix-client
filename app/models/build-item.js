import DS from 'ember-data';
import Base from 'phoenix/mixins/model';
import Measureable from 'phoenix/mixins/measureable-model';

export default DS.Model.extend(Base, Measureable, {
  build: DS.belongsTo('build'),
  item: DS.belongsTo('item')
});

import Ember from 'ember';
import DS from 'ember-data';
import Base from 'phoenix/mixins/model';
import Measureable from 'phoenix/mixins/measureable-model';

export default DS.Model.extend(Base, Measureable, {
  api: Ember.inject.service('api'),

  ingredient: DS.belongsTo('ingredient'),
  adjustments: DS.hasMany('adjustment'),
  nutrients: DS.hasMany('inventory-nutrient'),

  price: DS.attr('number'),
  lotNumber: DS.attr('string'),
  state: DS.attr('string'),
  available: DS.attr('number'),
  purchasedOn: DS.attr(),
  expiresOn: DS.attr(),

  isRecorded: Ember.computed.equal('state', 'recorded'),

  enterIntoRecord() {
    const url = `inventories/${this.get('id')}/record`;
    return this.get('api').put(url).then(() => {
      return this.reload();
    });
  }
});

import DS from 'ember-data';
import Builder from './builder';

export default Builder.extend({
  builds: DS.hasMany('build'),
  currentBuild: DS.belongsTo('build'),

  name: DS.attr(),
  labelName: DS.attr(),
  versions: DS.attr('number')
});

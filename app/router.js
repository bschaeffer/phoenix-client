import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('auth', function() {
    this.route('login');
    this.route('logout');
  });

  this.route('app', {path: 'app'}, function() {
    this.route('dashboard');

    this.route('ingredients', function() {
      this.route('show', {path: ':ingredient_id'});
      this.route('edit', {path: ':ingredient_id/edit'});
      this.route('new');
    });

    this.route('nutrients', function() {
      this.route('new');
      this.route('show', {path: ':nutrient_id'});
      this.route('edit', {path: ':nutrient_id/edit'});

      this.route('categories', function() {
        this.route('new');
        this.route('show', {path: ':category_id'});
        this.route('edit', {path: ':category_id/edit'});
      });
    });

    this.route('inventories', function() {
      this.route('new');
      this.route('show', {path: ':inventory_id'});
      this.route('edit', {path: ':inventory_id/edit'});
    });

    this.route('products', function() {
      this.route('new');
      this.route('show', {path: ':product_id'});
      this.route('edit', {path: ':product_id/edit'});
    });

    this.route('recipes', function() {
      this.route('new');
      this.route('show', {path: ':recipe_id'});
      this.route('edit', {path: ':recipe_id/edit'});
    });

    this.route('builds', function() {
      this.route('new', {path: ':builder_type/:builder_id/new'});
      this.route('show', {path: ':build_id'});
      this.route('edit', {path: ':build_id/edit'});
    });
  });
});

export default Router;

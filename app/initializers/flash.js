export function initialize(application) {
  application.inject('route', 'flash', 'service:flash');
}

export default {
  name: 'flash',
  initialize
};

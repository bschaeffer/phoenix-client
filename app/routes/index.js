import Ember from 'ember';

export default Ember.Route.extend({
  breadCrumb: null,

  beforeModel() {
    this.transitionTo('app.dashboard');
  }
});

import Ember from 'ember';

export function measureUnit(params) {
  const measure = params[0];

  if (!measure) {
    return 'ea';
  }

  const data = measure.toString().split(' ');

  if (data.length <= 1) {
    return 'ea';
  } else {
    return data[data.length-1];
  }
}

export default Ember.Helper.helper(measureUnit);

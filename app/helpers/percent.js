import Ember from 'ember';

export function percent(params, hash) {
  hash = hash || {};

  let value = params[0] || 0;
  let decimals = hash.decimals || 0;

  if (hash.convert) {
    value = value * 100;
  }

  return `${value.toFixed(decimals)}%`;
}

export default Ember.Helper.helper(percent);

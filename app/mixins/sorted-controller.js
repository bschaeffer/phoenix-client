import Ember from 'ember';

export default Ember.Mixin.create({
  /*
    Override this to set a default sort propery.
  */
  sort: null,

  /*
    The default sort order (asc).
  */
  order: 'asc',

  /*
    Generates a sorting definition used by Ember.computed.sort. An example might
    be `["name:desc", "age:desc"]` to sort by name, then age in descending
    order.
  */
  sortingDefinition: Ember.computed('sort', 'order', function() {
    let sort = this.get('sort');
    if (!Ember.isEmpty(sort)) {
      return [`${sort}:${this.get('order')}`];
    } else {
      return [];
    }
  }),

  /*
    The content to be sorted. Override this in the controller to prevent having
    to override `sortedContent`.
  */
  sortedContentProxy: Ember.computed.oneWay('model'),

  /*
    The sorted content. Reference this inside the controller or views.
  */
  sortedContent: Ember.computed.sort('sortedContentProxy', 'sortingDefinition')
});

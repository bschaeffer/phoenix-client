import Ember from 'ember';

export default Ember.Mixin.create({
  name: '',

  nameFilteredContent: function() {
    let name = this.get('name');

    let filtered = this.get('model');

    if (!Ember.isEmpty(name)) {
      const nameRegex = new RegExp(name, 'i');
      filtered = filtered.filter(function(ingredient) {
        return nameRegex.test(ingredient.get('name'));
      });
    }

    return filtered;
  }.property('model', 'name'),

  _mixinNameObserver: Ember.observer('name', function() {
    Ember.sendEvent(this, 'nameDidChange');
  })
});

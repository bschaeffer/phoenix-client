import Ember from 'ember';
import EditableTable from './table';

let {
  computed: { or },
  inject: { service }
} = Ember;

export default Ember.Mixin.create({
  store: service(),
  tagName: 'tr',
  classNameBindings: ['isEditing'],

  isEditing: false,
  isSaving: false,
  isDeleting: false,
  isDisabled: or('isSaving', 'isDeleting'),

  table: null,

  didInsertElement() {
    Ember.run.scheduleOnce('afterRender', this, 'registerWithTable');
    this._super(...arguments);
  },

  registerWithTable() {
    if (this.get('model.isNew')) {
      this.set('isEditing', true);
    }

    let table = this.nearestOfType(EditableTable);
    if (table) {
      this.set('table', table);
      table.get('tableRows').addObject(this);
    }
  },

  willDestroyElement() {
    let table = this.nearestOfType(ItemsTable);
    if (table) { table.get('tableRows').removeObject(this); }
    this._super(...arguments);
  },

  actions: {
    showEditor() {
      this.set('isEditing', true);
    }
  }
});

import Ember from 'ember';

let {
  computed,
  inject: { service }
} = Ember;

export default Ember.Mixin.create({
  store: service(),
  tableRows: computed(function() { return Ember.A(); }),

  isEditable: false,

  isEditing: computed('tableRows.@each.isEditing', function() {
    return this.get('tableRows').isAny('isEditing', true);
  }),

  isDisabled: computed('tableRows.@each.isDisabled', function() {
    return this.get('tableRows').isEvery('isDisabled', true);
  })
});

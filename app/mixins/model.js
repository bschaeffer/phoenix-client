import Ember from 'ember';
import DS from 'ember-data';

export default Ember.Mixin.create({
  createdAt: DS.attr(),
  updatedAt: DS.attr(),

  modelType: Ember.computed(function() {
    return Ember.get(this, 'constructor.modelName');
  })
});

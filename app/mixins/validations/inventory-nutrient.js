import Ember from 'ember';
import LazyValidations from 'phoenix/mixins/lazy-validations';

export default Ember.Mixin.create(LazyValidations, {
  validations: {
    'model.nutrient': {
      belongsTo: true
    },
    'model.inventory': {
      belongsTo: true
    },
    'model.measureValue': {
      presence: true,
      measured: 'nutrient'
    },
    'model.measureUnit': {
      presence: true,
      unitwise: true
    }
  }
});

import Ember from 'ember';
import pagedArray from 'ember-cli-pagination/computed/paged-array';

export default Ember.Mixin.create({
  /*
    The paginated query params. queryParams is a concatanatedProperty so we
    good.
  */
  queryParams: ['page', 'limit'],

  /*
    The default page.
  */
  page: 1,

  /*
    The default per-page limit.
  */
  limit: 10,

  limitOptions: Ember.computed(function() {
    return Ember.A([10, 20, 50]);
  }),

  /*
    Just use this for the content you want paginated. Makes it so we don't have
    to duplicate a pagedArray call when we mix this in.
  */
  pagedContentProxy: Ember.computed.oneWay('model'),

  /*
    The actual paginated content, bound to the correct query params.
  */
  pagedContent: pagedArray('pagedContentProxy', {
    pageBinding: 'page',
    perPageBinding: 'limit'
  }),

  limitChanged: Ember.observer('limit', function() {
    this.set('page', 1);
  })
});

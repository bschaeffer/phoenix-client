import Ember from 'ember';
import EmberValidations from 'ember-validations';
import Errors from 'ember-validations/errors';

export default Ember.Mixin.create(EmberValidations, {
  _hasValidated: false,

  fieldErrors: Ember.computed('_hasValidated', 'errors', function() {
    return this.get('_hasValidated') ? this.get('errors') : Errors.create({});
  }),

  cleanupValidations() {
    this.set('_hasValidated', false);
  },

  validate() {
    this.set('_hasValidated', true);
    return this._super(...arguments);
  }
});

import Ember from 'ember';
import DS from 'ember-data';
/* globals Qty */

export default Ember.Mixin.create({

  measureValue: DS.attr('number'),
  measureUnit: DS.attr(),

  measure: Ember.computed('measureUnit', 'measureValue', function() {
    let value = this.get('measureValue');
    if (Ember.isEmpty(value) || isNaN(value)) { value = 0; }
    try {
      return new Qty(Number(value), this.get('measureUnit'));
    } catch(error) {
      return new Qty(Number(value));
    }
  })
});

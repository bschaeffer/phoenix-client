import DS from 'ember-data';
import Ember from 'ember';
let {isArray, isEmpty} = Ember;

export default DS.JSONAPISerializer.extend({
  attrs: {
    createdAt: { serialize: false },
    updatedAt: { serialize: false }
  },

  keyForAttribute(attr) {
    return Ember.String.underscore(attr);
  },

  keyForRelationship(key) {
     return Ember.String.underscore(key);
  },

  serialize() {
    const json = this._super(...arguments);
    let atts = Ember.$.extend({}, json.data.attributes);

    if (json.data.hasOwnProperty('relationships')) {
      for (var key in json.data.relationships) {
        let relationship = json.data.relationships[key];
        if (!isArray(relationship.data) && !isEmpty(relationship.data)) {
          let name = Ember.String.underscore(`${key}_id`);
          atts[name] = relationship.data.id;
        }
      }
    }

    return atts;
  },

  serializeIntoHash(hash, type, snapshot, options) {
    var root = Ember.String.underscore(type.modelName);
    hash[root] = this.serialize(snapshot, options);
  }
});

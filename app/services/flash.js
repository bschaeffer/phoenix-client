import Ember from 'ember';
import DS from 'ember-data';

let messageAlias = function(type) {
  return function() {
    const messages = this.get('messages');
    messages[type].apply(messages, arguments);
  };
};

export default Ember.Service.extend({
  messages: Ember.inject.service('flashMessages'),

  success: messageAlias('success'),
  info: messageAlias('info'),
  warning: messageAlias('warning'),
  danger: messageAlias('danger'),

  displayAdapterError(error) {
    if (error instanceof DS.AdapterError) {
      let message = Ember.get(error, 'errors.firstObject.detail');

      if (Ember.isEmpty(message)) {
        message = 'There was an error performing that request.';
      }

      this.danger(message, {sticky: true});
    }
  }
});

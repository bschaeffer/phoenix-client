import Ember from 'ember';

export default Ember.Service.extend(Ember._ProxyMixin, Ember.Evented, {
  store: Ember.inject.service('store'),
  api: Ember.inject.service('api'),
  session: Ember.inject.service('local-settings'),

  isSignedIn: Ember.computed.notEmpty('content'),

  isLoggingOut: false,

  attemptedTransition: null,

  authHeaders: Ember.computed(function() {
    const tokens = this.get('session').getValue('tokens');
    return Ember.isEmpty(tokens) ? undefined : tokens;
  }).volatile(),

  login(user) {
    this.set('content', user);
    this.trigger('didLogin');
  },

  loginPayload(payload) {
    const id = payload.data.id;
    payload.data.type = 'user';

    const store = this.get('store');
    store.push(payload);
    this.login(store.peekRecord('user', id));
  },

  logout() {
    const api = this.get('api');
    let resolver = (resolve) => {
      this.clearSessionTokens();
      this.set('authHeaders', {});
      this.set('content', null);
      this.set('isLoggingOut', false);
      this.trigger('didLogout');
      resolve();
    };

    this.set('isLoggingOut', true);

    return new Ember.RSVP.Promise(function(resolve) {
      api.del('auth/sign_out').then(function() {
        resolver(resolve);
      }, function() {
        resolver(resolve);
      });
    });
  },

  authenticate(email, password) {
    const auth = this.get('api').post('auth/sign_in', {
      email: email,
      password: password
    });

    auth.then((payload) => {
      this.trigger('didAuthenticate');
      this.loginPayload(payload);
    });

    return auth;
  },

  ensure() {
    return new Ember.RSVP.Promise((resolve, reject) => {
      if (this.get('isSignedIn')) {
        resolve();
      } else if (this.loadSessionTokens()) {
        this.get('api').get('auth/validate_token.json').then((payload) => {
          this.loginPayload(payload);
          resolve();
        }, () => {
          this.clearSessionTokens();
          reject();
        });
      } else {
        this.clearSessionTokens();
        reject();
      }
    }, 'current-user: ensure');
  },

  loadSessionTokens() {
    const session = this.get('session');
    const tokens = session.getValue('tokens');
    const expiry = session.getValue('tokensExp');
    const currentTime = new Date().getTime() / 1000;

    if (tokens && expiry && expiry > currentTime) {
      this.set('authHeaders', tokens);
      return true;
    } else {
      return false;
    }
  },

  setSessionTokens(tokens, expiry) {
    const session = this.get('session');
    session.setValue('tokens', tokens);
    session.setValue('tokensExp', Number(expiry));
  },

  clearSessionTokens() {
    const session = this.get('session');
    session.setValue('tokens', null);
    session.setValue('tokensExp', null);
  },

  redirectOr(callback) {
    if (this.get('attemptedTransition')) {
      this.get('attemptedTransition').retry();
    } else {
      callback.call();
    }
  },

  setAuthTokensFromResposne(xhr) {
    if (xhr.getResponseHeader('access-token')) {
      const expiry = xhr.getResponseHeader('expiry');
      const tokens =  {
        'access-token': xhr.getResponseHeader('access-token'),
        'uid': xhr.getResponseHeader('uid'),
        'client': xhr.getResponseHeader('client')
      };

      this.setSessionTokens(tokens, expiry);
      this.set('authHeaders', tokens);
    }
  }
});

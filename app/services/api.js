import Ember from 'ember';
import ENV from 'phoenix/config/environment';
import AjaxService from 'ember-ajax/services/ajax';

let { get } = Ember;

let requestAlias = function(type) {
  return function(url, hash) {
    const options = {type: type};
    if (hash) { options.data = (hash.data) ? hash.data : hash; }
    return this.request(url, options);
  };
};

export default AjaxService.extend({
  currentUser: Ember.inject.service('current-user'),
  host: ENV.APP.apiEndpoint,

  headers: Ember.computed(function() {
    return get(this, 'currentUser.authHeaders');
  }).volatile(),

  get: requestAlias('GET'),
  post: requestAlias('POST'),
  put: requestAlias('PUT'),
  patch: requestAlias('PATCH'),
  del: requestAlias('DELETE'),

  normalizePath(url) {
    return this._buildUrl(url);
  },

  options() {
    const currentUser = get(this, 'currentUser');
    const hash = this._super(...arguments);
    hash.complete = function(xhr) {
      currentUser.setAuthTokensFromResposne(xhr);
    };

    return hash;
  }
});

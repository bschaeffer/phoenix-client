import Base from 'ember-validations/validators/base';
import Ember from 'ember';
import UNITS from 'phoenix/constants/unitwise-units';

export default Base.extend({
  call: function() {
    const value = this.model.get(this.property);
    if (!Ember.isEmpty(value) && !UNITS.contains(value)) {
      this.errors.pushObject('is not a valid unit of measure');
    }
  }
});

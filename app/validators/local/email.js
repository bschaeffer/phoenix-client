import Base from 'ember-validations/validators/base';
import Ember from 'ember';

export default Base.extend({
  call: function() {
    if (!Ember.isBlank(this.get(this.property))) {
      const tester = /^[A-Z0-9_\.%\+\-]+@(?:[A-Z0-9\-]+\.)+(?:[A-Z]{2,6})$/i;

      if (!tester.test(this.get(this.property))) {
        this.errors.pushObject('is not a valid email');
      }
    }
  }
});

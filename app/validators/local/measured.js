import Ember from 'ember';
import Numericality from 'ember-validations/validators/local/numericality';

export default Numericality.extend({
  init() {
    if (this.options.constructor !== String) {
      Ember.warn(
        'You must supply a valid relationship to check against',
        false,
        {id: 'phoenix.validators.local.measured'}
      );

      this.options = {};
      this.options.relationship = null;
    } else {
      let relationship = this.options;
      this.options = {};
      this.options.relationship = relationship;
    }

    this._super(...arguments);
    this.options.messages.numericality = `must be a numeric value`;
    this.options.messages.onlyInteger = `can only be integer counts for this ${this.options.relationship}`;
    this.options.messages.greaterThan = `must be positive`;
  },

  call() {
    if (Ember.isEmpty(this.options.relationship)) {
      return;
    }

    const relationship = this.get(`model.model.${this.options.relationship}`);

    if (Ember.isEmpty(relationship)) {
      return;
    }

    this.options.greaterThan = 0;

    if (!relationship.get('measureable')) {
      this.options.onlyInteger = true;
    }

    return this._super(...arguments);
  }
});

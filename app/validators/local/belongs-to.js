import Base from 'ember-validations/validators/base';
import Ember from 'ember';

export default Base.extend({
  call: function() {
    const model = this.get(`model.${this.property}`);
    if (Ember.isEmpty(model) || Ember.isEmpty(model.get('id'))) {
      this.errors.pushObject('must be present');
    }
  }
});

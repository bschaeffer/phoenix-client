import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:auth/login', 'Unit | Controller | login', {
  needs: [
    'service:validations',
    'ember-validations@validator:local/presence',
    'ember-validations@validator:local/length',
    'validator:local/email'
  ]
});

// Replace this with your real tests.
test('it has the correct defaults', function(assert) {
  let controller = this.subject();
  let props = controller.getProperties(
    'email',
    'password',
    'credentialsError',
    'isLoggingIn'
  );

  assert.equal(props.email, null);
  assert.equal(props.password, null);
  assert.equal(props.credentialsError, null);
  assert.equal(props.isLoggingIn, false);
});

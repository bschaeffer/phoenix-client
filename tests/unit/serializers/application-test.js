import Ember from 'ember';
import { moduleForModel, test } from 'ember-qunit';

moduleForModel('user', 'Unit | Serializer | application', {
  needs: ['serializer:application']
});

// Replace this with your real tests.
test('it serializes keys correctly', function(assert) {
  let record = this.subject();
  Ember.run(function() {
    record.set('firstName', 'Braden');
    let data = record.serialize();
    assert.equal(data.first_name, 'Braden');
  });
});

import { moduleForModel, test } from 'ember-qunit';

moduleForModel('build-item', 'Unit | Model | build item', {
  // Specify the other units that are required for this test.
  needs: [
    'model:item',
    'model:build',
    'model:builder'
  ]
});

test('it is measureable', function(assert) {
  let model = this.subject({
    measureValue: 1000,
    measureUnit: 'gram'
  });

  assert.ok(
    model.get('measure').hasOwnProperty('scalar'),
    'should act like Qty'
  );
});

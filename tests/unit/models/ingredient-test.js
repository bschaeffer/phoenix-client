import { moduleForModel, test } from 'ember-qunit';

moduleForModel('ingredient', 'Unit | Model | ingredient', {
  // Specify the other units that are required for this test.
  needs: [
    'model:inventory',
    'model:adjustment',
    'model:adjustable'
  ]
});

test('it exists', function(assert) {
  let model = this.subject();
  assert.ok(!!model);
});

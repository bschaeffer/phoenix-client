import { moduleForModel, test } from 'ember-qunit';

moduleForModel('nutrient-category', 'Unit | Model | nutrient category', {
  // Specify the other units that are required for this test.
  needs: [
    'model:nutrient'
  ]
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});

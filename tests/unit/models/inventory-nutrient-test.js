import Ember from 'ember';
import { moduleForModel, test } from 'ember-qunit';

moduleForModel('inventory-nutrient', 'Unit | Model | inventory nutrient', {
  // Specify the other units that are required for this test.
  needs: [
    'model:inventory',
    'model:adjustment',
    'model:ingredient',
    'model:nutrient'
  ]
});

test('it is measureable', function(assert) {
  let model = this.subject({
    measureValue: 1000,
    measureUnit: 'gram'
  });

  assert.ok(
    model.get('measure').hasOwnProperty('scalar'),
    'should act like Qty'
  );
});

test('it converts measures to the inventories', function(assert) {
  Ember.run(() => {
    let store = this.store();
    let inventory = store.createRecord('inventory', {
      measureValue: 1,
      measureUnit: 'lb'
    });

    let model = this.subject({
      inventory: inventory,
      measureValue: 1000,
      measureUnit: 'g'
    });

    const converted = model.get('measure').to(inventory.get('measure'));

    assert.equal(
      model.get('convertedMeasure'),
      converted,
      'converts the measure to '
    );
  });
});

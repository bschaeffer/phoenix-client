import { moduleForModel, test } from 'ember-qunit';

moduleForModel('nutrient', 'Unit | Model | nutrient', {
  // Specify the other units that are required for this test.
  needs: [
    'model:nutrient-category'
  ]
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});

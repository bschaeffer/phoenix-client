import Ember from 'ember';
import { moduleForModel, test } from 'ember-qunit';

moduleForModel('user', 'Unit | Model | user', {
  // Specify the other units that are required for this test.
  needs: []
});

test('it computes the full name', function(assert) {
  let model = this.subject();
  Ember.run(function() {
    model.setProperties({firstName: 'Test', lastName: 'User'});
  });

  assert.equal(model.get('fullName'), 'Test User');
});

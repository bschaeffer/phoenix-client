import { moduleForModel, test } from 'ember-qunit';

moduleForModel('inventory', 'Unit | Model | inventory', {
  // Specify the other units that are required for this test.
  needs: [
    'model:ingredient',
    'model:adjustment',
    'model:adjustable',
    'model:inventory-nutrient'
  ]
});

test('it is measureable', function(assert) {
  let model = this.subject({
    measureValue: 1000,
    measureUnit: 'gram'
  });

  assert.ok(
    model.get('measure').hasOwnProperty('scalar'),
    'should act like Qty'
  );
});

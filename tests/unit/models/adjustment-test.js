import { moduleForModel, test } from 'ember-qunit';

moduleForModel('adjustment', 'Unit | Model | adjustment', {
  needs: ['model:inventory', 'model:adjustable']
});

test('it exists', function(assert) {
  let model = this.subject();
  assert.ok(!!model);
});

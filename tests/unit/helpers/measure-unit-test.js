import { measureUnit } from '../../../helpers/measure-unit';
import { module, test } from 'qunit';

module('Unit | Helper | measure unit');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = measureUnit([42]);
  assert.ok(result);
});

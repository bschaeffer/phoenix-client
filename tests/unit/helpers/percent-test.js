import { percent } from '../../../helpers/percent';
import { module, test } from 'qunit';

module('Unit | Helper | percent');

// Replace this with your real tests.
test('it returns a percent string', function(assert) {
  let result = percent([42]);
  assert.equal(result, "42%");
});

module('given a convert option');
test('it multiples the number by 100', function(assert) {
  let result = percent([0.42], {convert: true});
  assert.equal(result, "42%");
});

module('given a decimal option');
test('it adds decimal places to the result percent', function(assert) {
  let result = percent([42.123456], {decimals: 5});
  assert.equal(result, "42.12346%");
});

import { capitalize } from '../../../helpers/capitalize';
import { module, test } from 'qunit';

module('Unit | Helper | capitalize');

test('it captializes strings', function(assert) {
  let result = capitalize(['help']);
  assert.equal(result, 'Help');
});

test('it handles non strings', function(assert) {
  let result = capitalize([42]);
  assert.equal(result, '42');
});

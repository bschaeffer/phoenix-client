// import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';
// import Pretender from 'pretender';

moduleFor('service:current-user', 'Unit | Service | current user', {
  needs: [
    'model:user',
    'service:api',
    'service:local-settings'
  ]
});

test('it has the correct defaults', function(assert) {
  let service = this.subject();
  assert.ok(!service.get('isSignedIn'));
  assert.ok(!service.get('isLoggingOut'));
});

test('it is signed in when there is any content', function(assert) {
  let service = this.subject();
  service.set('content', true);
  assert.ok(service.get('isSignedIn'));
});

test('it logs the user in', function(assert) {
  let service = this.subject();
  let data = {id: 45};

  service.login(data);
  assert.equal(service.get('id'), 45);
});

// test('it logs the user out', function(assert) {
//   new Pretender(function() {
//     this.delete('/auth/sign_out', function() {
//       return [200, {}, ''];
//     });
//   });
//
//   let service = this.subject();
//   service.login({example: 'data'});
//
//   assert.expect(4);
//
//   service.one('didLogout', function() {
//     assert.ok(true, 'triggers an event');
//   });
//
//   return service.logout().then(function() {
//     assert.equal(service.get('content'), null);
//     assert.equal(service.get('session').getValue('tokens'), undefined);
//     assert.equal(service.get('session').getValue('tokensExp'), undefined);
//   });
// });

// test('it authenticates, logs in the user, and sets tokens', function(assert) {
//   const data = {
//     type: 'users',
//     id: '1',
//     attributes: {
//       email: 'test@email.com'
//     }
//   };
//
//   const headers = {
//     'access-token': 'exampleToken',
//     'client-id': 'emberTester',
//     'uid': data.attributes.email,
//     'expiry': '1456789765959'
//   };
//
//   new Pretender(function() {
//     this.post('/auth/sign_in', function() {
//       return [200, headers, JSON.stringify({data})];
//     });
//   });
//
//   const expiry = Number(headers['expiry']);
//   const authHeaders = Ember.$.extend({}, headers);
//   delete authHeaders['expiry'];
//
//   assert.expect(5);
//
//   let service = this.subject();
//
//   service.one('didAuthenticate', function() {
//     assert.ok(true, 'triggers an event');
//   });
//
//   return service.authenticate('me@jbn.com', 'password').then(function() {
//     assert.ok(service.get('isSignedIn'));
//     assert.deepEqual(service.get('session').getValue('tokens'), authHeaders);
//     assert.equal(service.get('session').getValue('tokensExp'), expiry);
//     assert.equal(service.get('email'), data.attributes.email);
//   });
// });

import Ember from 'ember';
import CurrentUserInitializer from '../../../initializers/current-user';
import { module, test } from 'qunit';

let application;

module('Unit | Initializer | current user', {
  beforeEach() {
    Ember.run(function() {
      application = Ember.Application.create();
      application.deferReadiness();
    });
  }
});

test('it registers the current user', function(assert) {
  CurrentUserInitializer.initialize(application);
  assert.ok(true); // It works
});

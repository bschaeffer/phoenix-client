import Ember from 'ember';
import PagedControllerMixin from '../../../mixins/paged-controller';
import { module, test } from 'qunit';

module('Unit | Mixin | paged controller');

// Replace this with your real tests.
test('it works', function(assert) {
  let PagedControllerObject = Ember.Object.extend(PagedControllerMixin);
  let subject = PagedControllerObject.create();
  assert.ok(subject);
});

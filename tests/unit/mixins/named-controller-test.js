import Ember from 'ember';
import NamedControllerMixin from '../../../mixins/named-controller';
import { module, test } from 'qunit';

module('Unit | Mixin | named controller');

// Replace this with your real tests.
test('it works', function(assert) {
  let NamedControllerObject = Ember.Object.extend(NamedControllerMixin);
  let subject = NamedControllerObject.create();
  assert.ok(subject);
});

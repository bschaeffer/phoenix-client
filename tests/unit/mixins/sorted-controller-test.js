import Ember from 'ember';
import SortedControllerMixin from '../../../mixins/sorted-controller';
import { module, test } from 'qunit';

module('Unit | Mixin | sorted controller');

// Replace this with your real tests.
test('it works', function(assert) {
  let SortedControllerObject = Ember.Object.extend(SortedControllerMixin);
  let subject = SortedControllerObject.create();
  assert.ok(subject);
});

import Ember from 'ember';
import LazyValidationsMixin from '../../../mixins/lazy-validations';
import { module, test } from 'qunit';

module('Unit | Mixin | lazy validations');

// Replace this with your real tests.
test('it works', function(assert) {
  let LazyValidationsObject = Ember.Object.extend(LazyValidationsMixin);
  let subject = LazyValidationsObject.create();
  assert.ok(subject);
});

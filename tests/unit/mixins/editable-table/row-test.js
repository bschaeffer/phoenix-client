import Ember from 'ember';
import EditableTableRowMixin from 'phoenix/mixins/editable-table/row';
import { module, test } from 'qunit';

module('Unit | Mixin | editable table/row');

// Replace this with your real tests.
test('it works', function(assert) {
  let EditableTableRowObject = Ember.Object.extend(EditableTableRowMixin);
  let subject = EditableTableRowObject.create();
  assert.ok(subject);
});

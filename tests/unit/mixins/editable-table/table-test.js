import Ember from 'ember';
import EditableTableTableMixin from 'phoenix/mixins/editable-table/table';
import { module, test } from 'qunit';

module('Unit | Mixin | editable table/table');

// Replace this with your real tests.
test('it works', function(assert) {
  let EditableTableTableObject = Ember.Object.extend(EditableTableTableMixin);
  let subject = EditableTableTableObject.create();
  assert.ok(subject);
});

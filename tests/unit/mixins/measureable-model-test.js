import Ember from 'ember';
import MeasureableModelMixin from '../../../mixins/measureable-model';
import { module, test } from 'qunit';

module('Unit | Mixin | measureable model');

// Replace this with your real tests.
test('it works', function(assert) {
  let MeasureableModelObject = Ember.Object.extend(MeasureableModelMixin);
  let subject = MeasureableModelObject.create();
  assert.ok(subject);
});

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('twbs-boolean-label', 'Integration | Component | twbs boolean label', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{twbs-boolean-label}}`);
  assert.ok(true);
});

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('twbs-input', 'Integration | Component | twbs input', {
  integration: true
});

test('it renders correctly', function(assert) {
  this.render(hbs`{{twbs-input type='email' placeholder='Email'}}`);
  let $input = this.$('input');
  assert.equal($input.attr('type'), 'email');
  assert.equal($input.attr('placeholder'), 'Email');
  assert.ok($input.hasClass('form-control'));
});

test('it renders sizing correctly', function(assert) {
  this.set('sizing', 'lg');
  this.render(hbs`{{twbs-input sizing=sizing}}`);
  let $input = this.$('input');
  assert.ok($input.hasClass('form-control-lg'));
});

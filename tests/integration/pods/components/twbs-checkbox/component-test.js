import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('twbs-checkbox', 'Integration | Component | twbs checkbox', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{twbs-checkbox}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`
    {{#twbs-checkbox}}
      template block text
    {{/twbs-checkbox}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('twbs-btn', 'Integration | Component | twbs btn', {
  integration: true
});

test('it renders defaults correctly', function(assert) {
  this.render(hbs`{{twbs-btn}}`);

  const $button = this.$('button');
  assert.ok($button.hasClass('btn'));
  assert.ok($button.hasClass('btn-primary'));
  assert.notOk($button.attr('disabled'));
  assert.equal($button.text().trim(), 'Button');
});

test('it renders colors correctly', function(assert) {
  this.set('outline', false);
  this.set('color', 'primary');
  this.render(hbs`{{twbs-btn outline=outline color=color}}`);

  assert.ok(this.$('button').hasClass('btn-primary'));

  this.set('outline', true);
  assert.ok(this.$('button').hasClass('btn-primary-outline'));

  this.set('color', 'danger');
  assert.ok(this.$('button').hasClass('btn-danger-outline'));
});

test('it renders sizes correctly', function(assert) {
  this.set('sizing', 'sm');
  this.render(hbs`{{twbs-btn sizing=sizing}}`);

  assert.ok(this.$('button').hasClass('btn-sm'));

  this.set('sizing', 'lg');
  assert.ok(this.$('button').hasClass('btn-lg'));
});

test('it renders block buttons correctly', function(assert) {
  this.set('block', false);
  this.render(hbs`{{twbs-btn block=block}}`);

  assert.notOk(this.$('button').hasClass('btn-block'));

  this.set('block', true);
  assert.ok(this.$('button').hasClass('btn-block'));
});

test('it renders the type correctly', function(assert) {
  this.set('type', 'button');
  this.render(hbs`{{twbs-btn type=type}}`);

  assert.equal(this.$('button').attr('type'), 'button');

  this.set('type', 'submit');
  assert.equal(this.$('button').attr('type'), 'submit');
});

test('it handles disabled buttons', function(assert) {
  this.set('disabled', false);
  this.render(hbs`{{twbs-btn disabled=disabled}}`);

  assert.notOk(this.$('button').attr('disabled'), 'is not disabled');

  this.set('disabled', true);
  assert.ok(this.$('button').attr('disabled'), 'becomes disabled');
});

test('it handles active buttons', function(assert) {
  this.set('active', false);
  this.render(hbs`{{twbs-btn active=active}}`);

  assert.notOk(this.$('button').hasClass('active'));
  assert.equal(this.$('button').attr('aria-pressed'), 'false');

  this.set('active', true);
  assert.ok(this.$('button').hasClass('active'));
  assert.equal(this.$('button').attr('aria-pressed'), 'true');
});

test('it renders text correctly', function(assert) {
  this.set('disabled', false);
  this.set('text', 'Login');
  this.set('textDisabled', 'Logging In...');
  this.render(hbs`{{twbs-btn disabled=disabled text=text textDisabled=textDisabled}}`);

  assert.equal(this.$('button').text().trim(), 'Login');

  this.set('disabled', true);
  assert.equal(this.$('button').text().trim(), 'Logging In...');
});

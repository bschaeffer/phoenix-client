import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('inventory/nutrient-row', 'Integration | Component | inventory/nutrient-row', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{inventory/nutrient-row}}`);
  assert.ok(true);
});

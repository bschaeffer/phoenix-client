import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('inventory/nutrients-table', 'Integration | Component | inventory/nutrients-table', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{inventory/nutrients-table}}`);
  assert.ok(true);
});

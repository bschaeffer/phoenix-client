import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('build/item-row', 'Integration | Component | build/item-row', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{build/item-row}}`);
  assert.ok(true);
});

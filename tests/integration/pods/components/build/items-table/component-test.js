import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('build/items-table', 'Integration | Component | build/items table', {
  integration: true
});

test('it a message when empty', function(assert) {
  this.render(hbs`{{build/items-table}}`);

  const $el = this.$('div.alert');

  assert.ok($el.hasClass('alert'), 'should have alert class');
  assert.ok($el.hasClass('alert-sm'), 'should have alert-sm class');
  assert.ok($el.hasClass('bg-inverse'), 'should have bg-inverse class');
  assert.equal($el.text().trim(), 'No build items to display.');
});

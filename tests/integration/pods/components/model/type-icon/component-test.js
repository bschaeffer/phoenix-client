import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('model/type-icon', 'Integration | Component | model/type-icon', {
  integration: true
});

test('it renders the model type correctly', function(assert) {
  let model = Ember.Object.create({
    modelType: 'ingredient'
  });

  this.set('model', model);
  this.render(hbs`{{model/type-icon model}}`);

  assert.equal(this.$().text().trim(), 'I');
  assert.ok(this.$('div').hasClass('type-icon'));
  assert.ok(this.$('div').hasClass('type-icon-ingredient'));
});

test('it renders empty model types correctly', function(assert) {
  this.render(hbs`{{model/type-icon}}`);

  assert.equal(this.$().text().trim(), '');
  assert.ok(this.$('div').hasClass('type-icon'));
  assert.ok(this.$('div').hasClass('type-icon-undefined'));
});

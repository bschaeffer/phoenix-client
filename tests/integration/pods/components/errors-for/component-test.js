import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('errors-for', 'Integration | Component | errors for', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });"

  this.render(hbs`{{errors-for}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:"
  this.render(hbs`{{errors-for}}`);

  assert.equal(this.$().text().trim(), '');
});
